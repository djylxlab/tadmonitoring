###########################################################
##                   EXISTING RESOURCE                   ##
###########################################################

data "azurerm_resource_group" "rGroupTf" {
  name = var.rGroup
}

data "azurerm_mysql_flexible_server" "mysqlDatabaseTf" {
  name                = var.dbMaster
  resource_group_name = data.azurerm_resource_group.rGroupTf.name
}

data "azurerm_virtual_machine_scale_set" "vm1Tf" {
  name                = var.vm1
  resource_group_name = data.azurerm_resource_group.rGroupTf.name
}

###########################################################
##                   MONITORING                          ##
###########################################################

resource "azurerm_log_analytics_workspace" "example" {
  name                = "lAnalyticsTest2"
  location            = var.location
  resource_group_name = data.azurerm_resource_group.rGroupTf.name
  sku                 = "PerGB2018"
}

resource "azurerm_monitor_diagnostic_setting" "mysql_diagTf" {
  name                       = var.mysqlSettingName
  target_resource_id         = data.azurerm_mysql_flexible_server.mysqlDatabaseTf.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.example.id

  enabled_log {
    category = "MySqlSlowLogs"
  }
  metric {
    category = "AllMetrics"
  }
}


resource "azurerm_monitor_diagnostic_setting" "vm_diagnostic" {
  name                       = "vm-diag-settings2"
  target_resource_id         = data.azurerm_virtual_machine_scale_set.vm1Tf.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.example.id

  metric {
    category = "AllMetrics"
  }
}

resource "azurerm_log_analytics_solution" "workbook_solutionTf" {
  solution_name         = "workbook-solution"
  location              = var.location
  resource_group_name   = data.azurerm_resource_group.rGroupTf.name
  workspace_resource_id = azurerm_log_analytics_workspace.example.id
  workspace_name        = azurerm_log_analytics_workspace.example.name

  plan {
    publisher = "Microsoft"
    product   = "Workbook"
  }
}

###########################################################
##                       ALERTS                          ##
###########################################################

resource "azurerm_monitor_metric_alert" "cpuAlert" {
  name                = "cpu-high-alert"
  resource_group_name = data.azurerm_resource_group.rGroupTf.name
  scopes              = [data.azurerm_virtual_machine_scale_set.vm1Tf.id, ]

  criteria {
    metric_namespace = "Microsoft.Compute/virtualMachineScaleSets"
    metric_name      = "Percentage CPU"
    aggregation      = "Average"
    operator         = "GreaterThan"
    threshold        = 90
  }
  action {
    action_group_id = azurerm_monitor_action_group.alertEmailsTf.id
  }

  target_resource_location = var.location
  target_resource_type     = "Microsoft.Compute/virtualMachineScaleSets"
}

resource "azurerm_monitor_action_group" "alertEmailsTf" {
  name                = "alertEmails"
  resource_group_name = data.azurerm_resource_group.rGroupTf.name
  short_name          = "emailaction"

  email_receiver {
    name          = "Product_owner"
    email_address = "jacques.asdram@gmail.com"
  }
  email_receiver {
    name          = "Owner"
    email_address = "francois.asdram@gmail.com"
  }
  email_receiver {
    name          = "Big_chief"
    email_address = "blefevre@simplon.co"
  }
}

###########################################################
##                     AUTO SCALE                        ##
###########################################################

resource "azurerm_monitor_autoscale_setting" "aScaleTf" {
  name                = var.aScale
  resource_group_name = data.azurerm_resource_group.rGroupTf.name
  location            = var.location
  target_resource_id  = data.azurerm_virtual_machine_scale_set.vm1Tf.id
  enabled             = true

  profile {
    name = "defaultProfile"

    capacity {
      default = 1
      minimum = 1
      maximum = 8
    }

    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = data.azurerm_virtual_machine_scale_set.vm1Tf.id
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "GreaterThan"
        threshold          = 80
        metric_namespace   = "Microsoft.Compute/virtualMachineScaleSets"
        dimensions {
          name     = "WPapp"
          operator = "Equals"
          values   = ["App"]
        }
      }

      scale_action {
        direction = "Increase"
        type      = "ChangeCount"
        value     = "7"
        cooldown  = "PT1M"
      }
    }

    rule {
      metric_trigger {
        metric_name        = "Percentage CPU"
        metric_resource_id = data.azurerm_virtual_machine_scale_set.vm1Tf.id
        time_grain         = "PT1M"
        statistic          = "Average"
        time_window        = "PT5M"
        time_aggregation   = "Average"
        operator           = "LessThan"
        threshold          = 20
      }

      scale_action {
        direction = "Decrease"
        type      = "ChangeCount"
        value     = "7"
        cooldown  = "PT1M"
      }
    }
  }

  predictive {
    scale_mode      = "Enabled"
    look_ahead_time = "PT5M"
  }

  notification {
    email {
      send_to_subscription_administrator    = true
      send_to_subscription_co_administrator = true
      custom_emails                         = ["jacques.asdram@gmail.com"]
    }
  }
}
