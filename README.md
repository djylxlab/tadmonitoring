# TAD Monitoring

What is "TAD Monitoring" ? It's a Repository which contains an infrastructure as code (IaC) scripts to deploy a Secure Wordpress application on Microsoft Azure using Terraform, Docker and Ansible.
This infrastrucutre has been deployed as part of our Devops system administrator training course and done in a groups.
This project are worked in scrum mode and with **"Azure Devops"** for managing it.

Our group is composed to :
 - Jacques GARCIA (Product Owner)
 - François VIATGE (Owner)

## Deployment Overview

The aim of this deployment is to set up a secure application of your choice among Odoo, Jenkins or Wordpress (we choose Wordpress) using specific Azure tools.
Throughout this article, we will explain our deployment choices.

We had to realize it using the following azure components :

 - An Azure Database (Saas)
 - A SMB Storage
 - An Applciation Gateway
 - An Azure Key Vault
 - Protocol TLS
 - Azure Monitor
 - Scale Set 

The repository is composed of files and folders. For deploying your application you need all of them.

## Deployment Scripts and Automation

To deploy correctly our application, follow this instructions :
	
1. Run this command : **`terraform init`**
	- With it, terraform download the necessary provider's plug-in present in the file **`providers.tf`**, here **azurerm**.

2. Run this command : **`terraform apply -var-file=secrets.tfvars`**
	- With it, terraform use all the file **".tf"** to create a plan and execute it to deploy the infrastructure.
	- The **-var-file=secrets.tfvars** argument is used to specify that terraform should also use this file as a variable.
	- List of files and theirs descriptions :
		- **`secrets.tfvars`** : Is the secret variables file. Normally, we don't need to push this file on the repository for many security reasons. But, for the exercice we pushed it.
        - **`variables.tf`** : This file contains all the required variables to deploy our project.
		- **`main.tf`** : It's our main file which deploy all the infrastructure to our project. It deployes :

			- A **Resource group** : **OCC_ASD_EP01** 
				- modules : **azurerm_resource_group**
			- A **Virtual network** : **10.0.85.0/24** 
				- modules : **azurerm_virtual_network**
			- 4 **Subnets** in /26. 1 for the firewall, 1 for the application gateway, 1 for the virtuals machines and a last for the differents endpoints 
				- modules : **azurerm_subnet**
			- Retrieve the informations of our **account** 
				- modules : **azurerm_client_config**
			- An **Azure Key vault** for secrets managing and adding many secrets on its (database's password and access key storage account).
				- modules : **azurerm_key_vault** and **azurerm_key_vault_secret**
			- A **Linux Virtual Machine Scale Set** to have the possibility to auto scaling out it depending on CPU load. We choosen a linux image with docker already installed. With this module, we insert a bash custom script for deploying a wordpress container link to an endpoint (for the database).
				- modules : **azurerm_linux_virtual_machine_scale_set**
			- A **Public Ip** to use with the firewall 
				- modules : **azurerm_public_ip**
			- A **Firewall and its rules**  used for provisioning our virtuals machines. We have insert 2 ssh rules which allow the ssh protocol, 1 http protocol for testing and 1 rule which deny all the remaining traffic.
				- modules : **azurerm_firewall**, **azurerm_firewall_nat_rule_collection** and **azurerm_firewall_network_rule_collection**
			- An **Azure Database for Mysql flexible server**. We deploy a first server with a database and for the hight availability, a Replication to the first server. For data backup, there is an automatic backup every 7 days. Our databases is not in our virtual network. For using it with our container, we create an endpoint link its and a dns zone 
				- modules : **azurerm_msyql_flexible_server**, **azurerm_mysql_flexible_database**, **azurerm_private_endpoint**, **azurerm_private_dns_zons** and **azurerm_private_dns_zone_virtual_network_link**
			- A **Storage account** for volume mapping
				- modules : **azurerm_storage_account** and **azurerm_storage_share**
			- An **Application_Gateway and WAF** for traffic redirecting on port 80 and 443.
				- modules : **azurerm_application_gateway**

3. Go to ansible folder and run **`ansible-playbook playbook.yml`**
	- With it, we provisioning our virtual machine
	- List of files and theirs descriptions :
		- **`ansible.cfg`** : Default configuration file.
		- **`vars/main.yml`** : This file contains all the necessary variables to deploy our project.
		- **`hosts.ini`** : This file is empty but automatically feed with `playbook.yml`. It will contains the different host of our infrastructure.
		- **`playbook.yml`** : This file provision our virtual machine : 
			- In localhost, we retrieve the public ip for adding it in the host file.
				- modules : **azure_rm_publicipaddress_info**, **set_fact** and **debug**
			- We write on the host file the public ip and specify the good port
				- modules : **lineinfile**
			- Now, it's necessary to reload the inventory file.
				- modules : **meta**
			- We retrieve the access key of the storage account in the key vault
				- modules : **azure_rm_keyvault_info**, **set_fact**, **azure_rm_keyvaultsecret_info** and **debug**
			- In Host, we update the virtual machine and download some packages. We have choose a Rocky distribution so the package manager is dnf/yum
				- modules : **dnf**
			- We mount a volume in this virtual machine with the storage account with a bash command
				- modules : **shell**
			- For persistent volum mounting, we add a command on the "fstab" file
				- modules : **lineinfile**

4. Now, it's time to monitor it all. Go to **Monitoring** folder.

5. Run this command : **`terraform init`**
	- With it, terraform download the necessary provider present in the file **`providers.tf`**, here **azurerm**.

6.  Run this command : **`terraform apply -var-file=secrets.tfvars`**
	- With it, terraform use all the file **".tf"** to create a plan and execute it to deploy the infrastructure.
	- The **-var-file=secrets.tfvars** argument is used to specify that terraform should also use this file as a variable.
	- List of files and theirs descriptions :

		- **`secrets.tfvars`** : Is the secret variables file. Normally, we don't need push this file on the repository for many security reasons. But, for the exercice we push it.
		- **`variables.tf`** : This file contains all the required variables to deploy our project.
		- **`main.tf`** : This file deploy the monitoring for our project. It deployes :	
			- A **Log Analytics worskspace**. It's an azure service for monitoring as you want.
				- modules : **azurerm_log_analytics_workspace**
			- Adding **Database** and **Virtual machine** in this workspace analytics.
				- modules : **azurerm_monitor_diagnostic_setting**
			- Create a **Rule Metric Alert** which 	triggered at 90% CPU load of the virtual machine.
				- modules : **azurerm_monitor_metric_alert**
			- An **Action Group** which define which action is to be performed. Here, when the CPU load is Greater than 90%, an email have been send.
				- modules : **azurerm_monitor_action_group**
			- **Autoscaling**. When the CPU load is greater than 80%, the scale out of 7 virtuals machines is deployed.
				- modules : **azurerm_monitor_autoscale_setting**

Diagram of our infrastructure :

![Diagram](Diagramme.jpeg "Our Infrastructure")
