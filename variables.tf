###########################################################
##                   INFRASTRUCTURE                      ##
###########################################################

variable "rGroup" {
  type        = string
  default     = "OCC_ASD_EP01"
  description = "name of the resource group"
}

variable "location" {
  type        = string
  default     = "francecentral"
  description = "location of the resources"
}

variable "vNet" {
  type        = string
  default     = "Vnet_OCC_ASD_EP01"
  description = "Name of the virtual network"
}

variable "vNetAddress" {
  type    = list(string)
  default = ["10.0.85.0/24"]
}

variable "subNet1" {
  type        = string
  default     = "AzureFirewallSubnet"
  description = "Name of the first subnet"
}

variable "subNet1Address" {
  type    = list(string)
  default = ["10.0.85.0/26"]
}

variable "subNet2" {
  type        = string
  default     = "Subnet_OCC_ASD_EP01_1"
  description = "Name of the second subnet"
}

variable "subNet2Address" {
  type    = list(string)
  default = ["10.0.85.64/26"]
}

variable "subNet3" {
  type        = string
  default     = "Subnet_OCC_ASD_EP01_2"
  description = "Name of the third subnet"
}

variable "subNet3Address" {
  type    = list(string)
  default = ["10.0.85.128/26"]
}

variable "subNet4" {
  type        = string
  default     = "Subnet_OCC_ASD_EP01_3"
  description = "Name of the fourth subnet"
}

variable "subNet4Address" {
  type    = list(string)
  default = ["10.0.85.192/26"]
}

###########################################################
##                   VIRTUAL MACHINE                     ##
###########################################################

variable "vm1" {
  type        = string
  default     = "vmsswordpressEP01"
  description = "Name of the first vm"
}


variable "firstHostname" {
  type        = string
  default     = "wordpressEP01"
  description = "Hostname of the first vm"
}

variable "username" {
  type        = string
  default     = "fmj"
  description = "Name of the user"
}

###########################################################
##                       DATABASE                        ##
###########################################################

variable "dbMaster" {
  type        = string
  default     = "tad-mysql-mastertest7"
  description = "Name of the first database"
}

variable "dbAdmin" {
  type        = string
  default     = "adminMysql"
  description = "Login of the databse adminsitrator"
}

variable "mysql_password" {
  type        = string
  description = "Password of the database adminsitrator"
  sensitive   = true
}

variable "dnsName" {
  type        = string
  default     = "mysqldatabasestest.private.mysql.database.azure.com"
  description = "Name of the dns zone"
}

variable "link" {
  type        = string
  default     = "VnetZonetest.com"
  description = "Name of the link"
}

variable "dbEndpoint" {
  type        = string
  default     = "private_endpoint-sqltest"
  description = "Name of the endpoint"
}
variable "dbName" {
  type        = string
  default     = "wordpress"
  description = "Name of the database"
}

variable "dbReplica" {
  type        = string
  default     = "mysql-replica-test7"
  description = "Name of the replica"
}

###########################################################
##                   AZURE FIREWALL                      ##
###########################################################

variable "ipFw" {
  type        = string
  default     = "azureFirewallIpTest"
  description = "Name of the ip for the firewall"
}

variable "azureFirewallName" {
  type        = string
  default     = "FirewallTest"
  description = "Name of the firewall"
}

###########################################################
##                   STORAGE ACCOUN                      ##
###########################################################

variable "sAccount" {
  type        = string
  default     = "mystorageep01"
  description = "Name of the storage account"
}

###########################################################
##                   AZURE KEY VAULT                     ##
###########################################################

variable "kVault" {
  type        = string
  default     = "myvaultEP01"
  description = "Name of our Azure Key Vault"
}

variable "secret1Name" {
  type        = string
  default     = "mysql-passwdtest7"
  description = "Name of the secret in the Key vault"
}

variable "secret2Name" {
  type        = string
  default     = "accessstoreEP01"
  description = "Name of the second secret in the key vault"
}

###########################################################
##                   APPLICATION GATEWAY                 ##
###########################################################

variable "agName" {
  type        = string
  default     = "Application_Gateway_EP01"
  description = "Name of the application gateway"
}

variable "subscription_id" {
  type        = string
  description = "Subscritpion ID"
  sensitive   = true
}

