###########################################################
##                   INFRASTRUCTURE                      ##
###########################################################
resource "azurerm_resource_group" "rGroupTf" {
  name     = var.rGroup
  location = var.location
}

resource "azurerm_virtual_network" "vNetTf" {
  name                = var.vNet
  location            = var.location
  resource_group_name = azurerm_resource_group.rGroupTf.name
  address_space       = var.vNetAddress

}

resource "azurerm_subnet" "subNet1Tf" {
  name                 = var.subNet1
  resource_group_name  = azurerm_resource_group.rGroupTf.name
  virtual_network_name = azurerm_virtual_network.vNetTf.name
  address_prefixes     = var.subNet1Address
}

resource "azurerm_subnet" "subNet2Tf" {
  name                 = var.subNet2
  resource_group_name  = azurerm_resource_group.rGroupTf.name
  virtual_network_name = azurerm_virtual_network.vNetTf.name
  address_prefixes     = var.subNet2Address
}

resource "azurerm_subnet" "subNet3Tf" {
  name                 = var.subNet3
  resource_group_name  = azurerm_resource_group.rGroupTf.name
  virtual_network_name = azurerm_virtual_network.vNetTf.name
  address_prefixes     = var.subNet3Address
}

resource "azurerm_subnet" "subNet4Tf" {
  name                 = var.subNet4
  resource_group_name  = azurerm_resource_group.rGroupTf.name
  virtual_network_name = azurerm_virtual_network.vNetTf.name
  address_prefixes     = var.subNet4Address
}

###########################################################
##                   CURRENT CLIENT                      ##
###########################################################

data "azurerm_client_config" "current" {
}

###########################################################
##                   AZURE KEY VAULT                     ##
###########################################################

resource "azurerm_key_vault" "kVaultTf" {
  name                        = var.kVault
  location                    = var.location
  resource_group_name         = azurerm_resource_group.rGroupTf.name
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false

  sku_name = "standard"

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [ 
      "Backup",
      "Create",
      "Decrypt",
      "Delete",
      "Encrypt",
      "Get",
      "Import",
      "List",
      "Purge",
      "Recover",
      "Restore",
      "Sign",
      "UnwrapKey",
      "Update",
      "Verify",
      "WrapKey",
      "Release",
      "Rotate",
      "GetRotationPolicy",
      "SetRotationPolicy"
    ]
    secret_permissions = [
      "Backup",
      "Delete",
      "Get",
      "List",
      "Purge",
      "Recover",
      "Restore",
      "Set",
    ]
    storage_permissions = [
      "Backup",
      "Delete",
      "DeleteSAS",
      "Get",
      "GetSAS",
      "List",
      "ListSAS",
      "Purge",
      "Recover",
      "RegenerateKey",
      "Restore",
      "Set",
      "SetSAS",
      "Update"
    ]
    certificate_permissions = [
      "Backup",
      "Create",
      "Delete",
      "DeleteIssuers",
      "Get",
      "GetIssuers",
      "Import",
      "List",
      "ListIssuers",
      "ManageContacts",
      "ManageIssuers",
      "Purge",
      "Recover",
      "Restore",
      "SetIssuers",
      "Update"
    ]
  }
}

# Add a secret for databases
resource "azurerm_key_vault_secret" "secret1Tf" {
  name         = var.secret1Name
  value        = var.mysql_password
  key_vault_id = azurerm_key_vault.kVaultTf.id
}

###########################################################
##                   VIRTUALS MACHINES                   ##
###########################################################

resource "azurerm_linux_virtual_machine_scale_set" "vm1Tf" {
  name                = var.vm1
  location            = var.location
  resource_group_name = azurerm_resource_group.rGroupTf.name
  sku                 = "Standard_B2s"
  instances           = 1
  admin_username      = var.username
  upgrade_mode        = "Automatic"

  network_interface {
    name    = "nicVmss1"
    primary = true

    ip_configuration {
      name                                         = "internal"
      subnet_id                                    = azurerm_subnet.subNet3Tf.id
      application_gateway_backend_address_pool_ids = ["/subscriptions/${var.subscription_id}/resourceGroups/${var.rGroup}/providers/Microsoft.Network/applicationGateways/${var.agName}/backendAddressPools/backend_address_pool_FMJ"]
    }
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "tidalmediainc"
    offer     = "docker-compose-rocky-9"
    sku       = "docker-compose-rocky-9"
    version   = "1.0.0"
  }

  plan {
    publisher = "tidalmediainc"
    product   = "docker-compose-rocky-9"
    name      = "docker-compose-rocky-9"
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  extension {
    name                 = "customScript"
    publisher            = "Microsoft.Azure.Extensions"
    type                 = "CustomScript"
    type_handler_version = "2.0"
    settings             = <<SETTINGS
    {
      "commandToExecute": "sudo mkdir wp_data && sudo docker run -d -p 80:80 --name mywordpress -e WORDPRESS_DB_HOST=10.0.85.197 -e WORDPRESS_DB_USER=adminMysql -e WORDPRESS_DB_NAME=wordpress -e WORDPRESS_DB_PASSWORD=admin120686! -v /home/fmj/wp_data/:/var/www/html wordpress:latest"
    }
    SETTINGS
  }

  depends_on = [
    azurerm_application_gateway.aGatewayTf
  ]
}


##########################################
#                                        #
# Virtual machine to deal with certbot   #
#                                        #
##########################################


resource "azurerm_network_interface" "niccertbot" {
  name                = "niccertbot"
  location            = var.location
  resource_group_name = var.rGroup

  ip_configuration {
    name                          = "nicIP"
    subnet_id                     = azurerm_subnet.subNet3Tf.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "vmcertbot" {
  name                = "vmcertbot"
  resource_group_name = var.rGroup
  location            = var.location
  size                = "Standard_B2s"
  admin_username      = "fmj"
  network_interface_ids = [
    azurerm_network_interface.niccertbot.id,
  ]

  admin_ssh_key {
    username   = "fmj"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }
}


 ####################################
 #                                  #
 # links Application gateway to     #
 #       network interfaces         #
 ####################################



#VM1
resource "azurerm_network_interface_application_gateway_backend_address_pool_association" "link" {
  network_interface_id    = azurerm_network_interface.niccertbot.id
  ip_configuration_name   = "nicIP"
  backend_address_pool_id = tolist(azurerm_application_gateway.aGatewayTf.backend_address_pool).0.id
}

###########################################################
##                         FIREWALL                      ##
###########################################################

resource "azurerm_public_ip" "ipFwTf" {
  name                = var.ipFw
  location            = var.location
  resource_group_name = azurerm_resource_group.rGroupTf.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_firewall" "azureFirewallTf" {
  name                = var.azureFirewallName
  location            = var.location
  resource_group_name = azurerm_resource_group.rGroupTf.name
  sku_name            = "AZFW_VNet"
  sku_tier            = "Standard"

  ip_configuration {
    name                 = "configurationFw"
    subnet_id            = azurerm_subnet.subNet1Tf.id
    public_ip_address_id = azurerm_public_ip.ipFwTf.id
  }
}

resource "azurerm_firewall_nat_rule_collection" "natRuleFw1Tf" {
  name                = "natRuleSsh1"
  azure_firewall_name = azurerm_firewall.azureFirewallTf.name
  resource_group_name = azurerm_resource_group.rGroupTf.name
  priority            = 100
  action              = "Dnat"

  rule {
    name = "sshVm1"

    source_addresses = [
      "*",
    ]

    destination_ports = [
      "221",
    ]

    destination_addresses = [
      azurerm_public_ip.ipFwTf.ip_address
    ]

    translated_port = 22

    translated_address = "10.0.85.132"

    protocols = [
      "TCP",
    ]
  }


}
resource "azurerm_firewall_nat_rule_collection" "natRuleFw2Tf" {
  name                = "natRuleSsh2"
  azure_firewall_name = azurerm_firewall.azureFirewallTf.name
  resource_group_name = azurerm_resource_group.rGroupTf.name
  priority            = 110
  action              = "Dnat"

  rule {
    name = "sshVmss"

    source_addresses = [
      "*",
    ]

    destination_ports = [
      "222",
    ]

    destination_addresses = [
      azurerm_public_ip.ipFwTf.ip_address
    ]

    translated_port = 22

    translated_address = "10.0.85.134"

    protocols = [
      "TCP",
    ]
  }
}


resource "azurerm_firewall_nat_rule_collection" "sshcertbotvm" {
   name                = "sshcertbotvm"
   azure_firewall_name = azurerm_firewall.azureFirewallTf.name
   resource_group_name = var.rGroup
   priority            = 115
   action              = "Dnat"

   rule {
     name = "sshcertbotvm"

     source_addresses = [
       "*",
     ]

     destination_ports = [
       "223",
     ]
     destination_addresses = [
      azurerm_public_ip.ipFwTf.ip_address
     ]

     translated_port = 22

     translated_address = azurerm_linux_virtual_machine.vmcertbot.private_ip_address

     protocols = [
       "TCP",
     ]
   }
 }


resource "azurerm_firewall_nat_rule_collection" "natRuleFw3Tf" {
  name                = "natRuleHttp"
  azure_firewall_name = azurerm_firewall.azureFirewallTf.name
  resource_group_name = azurerm_resource_group.rGroupTf.name
  priority            = 120
  action              = "Dnat"

  rule {
    name = "httpVm1"

    source_addresses = [
      "*",
    ]

    destination_ports = [
      "80",
    ]

    destination_addresses = [
      azurerm_public_ip.ipFwTf.ip_address
    ]

    translated_port = 80

    translated_address = "10.0.85.132"

    protocols = [
      "TCP",
    ]
  }
}

resource "azurerm_firewall_network_rule_collection" "ruleFw4Tf" {
  name = "BlockAll"
  azure_firewall_name = azurerm_firewall.azureFirewallTf.name
  resource_group_name = azurerm_resource_group.rGroupTf.name
  priority = 130
  action = "Deny"

  rule {
    name = "Block"
    
    source_addresses = [
      "*",
    ]
   
    destination_ports = [
      "*",
    ]

    destination_addresses = [
      azurerm_public_ip.ipFwTf.ip_address
    ]

    protocols = [
      "TCP","UDP",
    ]
  }
}
###########################################################
##                       DATABASE                        ##
###########################################################
resource "azurerm_mysql_flexible_server" "dbMasterTf" {
  name                   = var.dbMaster
  resource_group_name    = azurerm_resource_group.rGroupTf.name
  location               = var.location
  administrator_login    = var.dbAdmin
  administrator_password = var.mysql_password
  backup_retention_days  = 7
  sku_name               = "GP_Standard_D2ds_v4"
  version                = "8.0.21"
}

resource "azurerm_mysql_flexible_database" "dbNameTf" {
  name                = var.dbName
  resource_group_name = azurerm_resource_group.rGroupTf.name
  server_name         = azurerm_mysql_flexible_server.dbMasterTf.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

resource "azurerm_mysql_flexible_server" "dbReplicaTf" {
  name                   = var.dbReplica
  resource_group_name    = azurerm_resource_group.rGroupTf.name
  location               = var.location
  administrator_login    = var.dbAdmin
  administrator_password = var.mysql_password
  backup_retention_days  = 7
  sku_name               = "GP_Standard_D2ds_v4"
  version                = "8.0.21"
  create_mode            = "Replica"
  source_server_id       = azurerm_mysql_flexible_server.dbMasterTf.id
}

resource "azurerm_private_endpoint" "dbEndpointTf" {
  name                = var.dbEndpoint
  location            = var.location
  resource_group_name = azurerm_resource_group.rGroupTf.name
  subnet_id           = azurerm_subnet.subNet4Tf.id

  private_service_connection {
    name                           = "private-serviceconnection"
    private_connection_resource_id = azurerm_mysql_flexible_server.dbMasterTf.id
    subresource_names              = ["mysqlServer"]
    is_manual_connection           = false
  }

  private_dns_zone_group {
    name                 = "dns_zone_group"
    private_dns_zone_ids = [azurerm_private_dns_zone.zDnsTf.id]
  }
}

resource "azurerm_private_dns_zone" "zDnsTf" {
  name                = var.dnsName
  resource_group_name = azurerm_resource_group.rGroupTf.name
}

resource "azurerm_private_dns_zone_virtual_network_link" "dnsLink" {
  name                  = "dns-link"
  resource_group_name   = azurerm_resource_group.rGroupTf.name
  private_dns_zone_name = azurerm_private_dns_zone.zDnsTf.name
  virtual_network_id    = azurerm_virtual_network.vNetTf.id
}
###########################################################
##               STORAGE ACCOUNT FOR MAP                 ##
###########################################################

resource "azurerm_storage_account" "sAccountTf" {
  name                     = var.sAccount
  resource_group_name      = azurerm_resource_group.rGroupTf.name
  location                 = var.location
  account_tier             = "Premium"
  account_replication_type = "LRS"
  account_kind             = "FileStorage"
}

resource "azurerm_private_endpoint" "pEndpointShareTf" {
  name                = "pEndpointShare"
  location            = var.location
  resource_group_name = azurerm_resource_group.rGroupTf.name
  subnet_id           = azurerm_subnet.subNet4Tf.id

  private_service_connection {
    name                           = "storage"
    private_connection_resource_id = azurerm_storage_account.sAccountTf.id
    is_manual_connection           = false
    subresource_names              = ["File"]
  }
}

resource "azurerm_storage_share" "sShareTadTf" {
  name                 = "ssharetad"
  storage_account_name = azurerm_storage_account.sAccountTf.name
  quota                = 100
}

resource "azurerm_private_dns_zone" "secondDnsZoneTf" {
  name                = "privatelinktest.file.core.windows.net"
  resource_group_name = azurerm_resource_group.rGroupTf.name
}

resource "azurerm_private_dns_zone_virtual_network_link" "dnsLink2" {
  name                  = "dns-link2"
  resource_group_name   = azurerm_resource_group.rGroupTf.name
  private_dns_zone_name = azurerm_private_dns_zone.secondDnsZoneTf.name
  virtual_network_id    = azurerm_virtual_network.vNetTf.id
}

# Add key's storage to the vault

resource "azurerm_key_vault_secret" "secret2Tf" {
  name         = var.secret2Name
  value        = azurerm_storage_account.sAccountTf.primary_access_key
  key_vault_id = azurerm_key_vault.kVaultTf.id
}
####################################
#                                  #
#  Address IP Public App Gateway   #
#                                  #
####################################

resource "azurerm_public_ip" "IpAppGateway" {
  name                = "IpAppGateway"
  resource_group_name = var.rGroup
  location            = var.location
  allocation_method   = "Static"
  sku                 = "Standard"
  zones               = ["1", "2", "3"]
  tags = {
    name = var.rGroup
  }

  depends_on = [
    azurerm_resource_group.rGroupTf
  ]
}


####################################
#                                  #
#   Creation GAteway application   #
#                                  #
####################################


resource "azurerm_application_gateway" "aGatewayTf" {
  name                = var.agName
  resource_group_name = var.rGroup
  location            = var.location
  zones               = ["1", "2", "3"]

  sku {
    name     = "WAF_v2"
    tier     = "WAF_v2"
    capacity = 2
  }

  gateway_ip_configuration {
    name      = "my-gateway-ip-configuration"
    subnet_id = azurerm_subnet.subNet2Tf.id
  }

  frontend_port {
    name = "http"
    port = 80
  }

  frontend_port {
    name = "https"
    port = 443
  }

  frontend_ip_configuration {
    name                 = "feIP"
    public_ip_address_id = azurerm_public_ip.IpAppGateway.id
  }
  backend_address_pool {
    name = "backend_address_pool_FMJ"
  }

  backend_http_settings {
    name                  = "http_setting_FMJ"
    cookie_based_affinity = "Disabled"
    path                  = "/"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 60
  }

  http_listener {
    name                           = "listener_http_FMJ"
    frontend_ip_configuration_name = "feIP"
    frontend_port_name             = "http"
    protocol                       = "Http"
  }

  request_routing_rule {
    name                       = "http_to_http_backend"
    priority                   = 9
    rule_type                  = "Basic"
    http_listener_name         = "listener_http_FMJ"
    backend_address_pool_name  = "backend_address_pool_FMJ"
    backend_http_settings_name = "http_setting_FMJ"
  }



  waf_configuration {
    enabled          = true
    firewall_mode    = "Detection"
    rule_set_type    = "OWASP"
    rule_set_version = "3.2"
  }

  depends_on = [
    azurerm_firewall.azureFirewallTf
  ]
}

####################################
#                                  #
#       DNS wich public zone       #
#                                  #
####################################


resource "azurerm_dns_zone" "dns" {
  name                = "fmjsimplon.ddns.net"
  resource_group_name = var.rGroup
}
